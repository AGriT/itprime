#include <iostream>
#include <unordered_map>
#include <numeric>
#include <vector>

using namespace std;

static const int sc_nMaxNumberValue = 12;
static const int sc_nHalfNumbers = 6;
static const int sc_nSystem = 13;

enum class eReturnCode
{
	eRC_OK = 0,
	eRC_Overflow
};

eReturnCode eIncreaseNumber(vector<int>&, int);

eReturnCode eShift(vector<int>& vnShifted, int nPos)
{
	vnShifted[nPos] = 0;

	return eIncreaseNumber(vnShifted, --nPos);
}

eReturnCode eIncreaseNumber(vector<int>& vnIncreased, int nPos)
{
	if (vnIncreased[nPos] != sc_nMaxNumberValue)
	{
		vnIncreased[nPos]++;
		return eReturnCode::eRC_OK;
	}

	if (nPos != 0)
		return eShift(vnIncreased, nPos);

	return eReturnCode::eRC_Overflow;
}

int main()
{
	vector<int> vSixNum;
	vSixNum.resize(sc_nHalfNumbers);
	unordered_map<int, uint64_t> mpSummCount;

	do
	{
		int nSumm = accumulate(vSixNum.begin(), vSixNum.end(), 0);

		mpSummCount[nSumm]++;

	} while (eIncreaseNumber(vSixNum, 5) != eReturnCode::eRC_Overflow);

	uint64_t unPrettyNumbers = 0;

	for (auto& it : mpSummCount)
		unPrettyNumbers += it.second * it.second;

	unPrettyNumbers *= sc_nSystem;

	cout << unPrettyNumbers;

	return 0;
}